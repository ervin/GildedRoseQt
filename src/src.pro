TARGET = GildedRoseQt
TEMPLATE = app

QT += widgets

SOURCES += main.cpp \
    gildedrosewindow.cpp

HEADERS += \
    gildedrosewindow.h

FORMS += \
    gildedrosewindow.ui
