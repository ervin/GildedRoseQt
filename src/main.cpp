#include <QApplication>

#include "gildedrosewindow.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    GildedRoseWindow window;

    window.addItem("+5 Dexterity Vest", 10, 20);
    window.addItem("Aged Brie", 2, 0);
    window.addItem("Elixir of the Mongoose", 5, 7);
    window.addItem("Sulfuras, Hand of Ragnaros", 0, 80);
    window.addItem("Sulfuras, Hand of Ragnaros", -1, 80);
    window.addItem("Backstage passes to a TAFKAL80ETC concert", 15, 20);
    window.addItem("Backstage passes to a TAFKAL80ETC concert", 10, 49);
    window.addItem("Backstage passes to a TAFKAL80ETC concert", 5, 49);
    // this Conjured item doesn't yet work properly
    window.addItem("Conjured Mana Cake", 3, 6);

    window.resize(480, 480);
    window.show();
    return app.exec();
}
