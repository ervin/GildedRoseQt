#include "gildedrosewindow.h"
#include "ui_gildedrosewindow.h"

GildedRoseWindow::GildedRoseWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GildedRoseWindow)
{
    ui->setupUi(this);
    connect(ui->updateButton, &QPushButton::clicked,
            this, &GildedRoseWindow::updateQuality);
}

GildedRoseWindow::~GildedRoseWindow()
{
    delete ui;
}

QList<GildedRoseWindow::Item> GildedRoseWindow::items() const
{
    return m_items;
}

void GildedRoseWindow::addItem(const Item &item)
{
    m_items << item;

    ui->tableWidget->clear();
    ui->tableWidget->setRowCount(m_items.size());
    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Name" << "Sell In" << "Quality");

    int row = 0;
    foreach (const Item &item, m_items) {
        ui->tableWidget->setItem(row, 0, new QTableWidgetItem(item.name));
        ui->tableWidget->setItem(row, 1, new QTableWidgetItem(QString::number(item.sellIn)));
        ui->tableWidget->setItem(row, 2, new QTableWidgetItem(QString::number(item.quality)));

        row++;
    }

    ui->tableWidget->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
}

void GildedRoseWindow::addItem(const QString &name, int sellIn, int quality)
{
    addItem(Item(name, sellIn, quality));
}

void GildedRoseWindow::updateQuality()
{
    for (int i = 0; i < m_items.size(); i++) {
        if (m_items[i].name != "Aged Brie" && m_items[i].name != "Backstage passes to a TAFKAL80ETC concert") {
            if (m_items[i].quality > 0) {
                if (m_items[i].name != "Sulfuras, Hand of Ragnaros") {
                    m_items[i].quality = m_items[i].quality - 1;
                }
            }
        } else {
            if (m_items[i].quality < 50) {
                m_items[i].quality = m_items[i].quality + 1;

                if (m_items[i].name == "Backstage passes to a TAFKAL80ETC concert") {
                    if (m_items[i].sellIn < 11) {
                        if (m_items[i].quality < 50) {
                            m_items[i].quality = m_items[i].quality + 1;
                        }
                    }

                    if (m_items[i].sellIn < 6) {
                        if (m_items[i].quality < 50) {
                            m_items[i].quality = m_items[i].quality + 1;
                        }
                    }
                }
            }
        }

        if (m_items[i].name != "Sulfuras, Hand of Ragnaros") {
            m_items[i].sellIn = m_items[i].sellIn - 1;
        }

        if (m_items[i].sellIn < 0) {
            if (m_items[i].name != "Aged Brie") {
                if (m_items[i].name != "Backstage passes to a TAFKAL80ETC concert") {
                    if (m_items[i].quality > 0) {
                        if (m_items[i].name != "Sulfuras, Hand of Ragnaros") {
                            m_items[i].quality = m_items[i].quality - 1;
                        }
                    }
                } else {
                    m_items[i].quality = m_items[i].quality - m_items[i].quality;
                }
            } else {
                if (m_items[i].quality < 50) {
                    m_items[i].quality = m_items[i].quality + 1;
                }
            }
        }
    }

    ui->tableWidget->clear();
    ui->tableWidget->setRowCount(m_items.size());
    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Name" << "Sell In" << "Quality");

    int row = 0;
    foreach (const Item &item, m_items) {
        ui->tableWidget->setItem(row, 0, new QTableWidgetItem(item.name));
        ui->tableWidget->setItem(row, 1, new QTableWidgetItem(QString::number(item.sellIn)));
        ui->tableWidget->setItem(row, 2, new QTableWidgetItem(QString::number(item.quality)));

        row++;
    }

    ui->tableWidget->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
}
