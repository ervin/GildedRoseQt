#ifndef GILDEDROSEWINDOW_H
#define GILDEDROSEWINDOW_H

#include <QList>
#include <QWidget>

namespace Ui {
class GildedRoseWindow;
}

class GildedRoseWindow : public QWidget
{
    Q_OBJECT

public:
    class Item
    {
    public:
        Item(const QString &name, int sellIn, int quality)
            : name(name), sellIn(sellIn), quality(quality) {}

        QString name;
        int sellIn;
        int quality;
    };

    explicit GildedRoseWindow(QWidget *parent = 0);
    ~GildedRoseWindow();

    QList<Item> items() const;

    void addItem(const Item &item);
    void addItem(const QString &name, int sellIn, int quality);

private slots:
    void updateQuality();

private:
    Ui::GildedRoseWindow *ui;
    QList<Item> m_items;
};

#endif // GILDEDROSEWINDOW_H
